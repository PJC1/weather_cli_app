const ora = require('ora');
const getWeather = require('../utils/weatherRequest');


module.exports = async (args) => {
  // build an object of flags used to pass as an argument to getWeather
  let obj = {};

  // start loading indicator
  const spinner = ora().start();

  try {
    // handle if temp flag is provided
    if(args.hasOwnProperty('temp') || args.hasOwnProperty('t')) {
      // cache temp from args
      const temp = args.temp || args.t;
      // add 'temp' key to the storage object
      obj['temp'] = temp;
    }

    // handle if zipcode flag is provided
    if(args.hasOwnProperty('zipcode') || args.hasOwnProperty('z')) {
      // cache zipcode from args
      const zipcode = args.zipcode || args.z;
      // add 'zipcode' key to the storage object
      obj['zipcode'] = zipcode;
      // call the weather request function from weatherRequest.js
      const weather = await getWeather(obj);
      // stop loading indicator
      spinner.stop();
      // log the returned temp
      console.log(weather);
    }

    // handle if city flag is provided
    if(args.hasOwnProperty('city') || args.hasOwnProperty('c')) {
      // cache city from args
      const city = args.city || args.c;
      // add 'city' key to the storage object
      obj['city']=city;
      // call the weather request function from weatherRequest.js
      const weather = await getWeather(obj);
      // stop loading indicator
      spinner.stop();
      // log the returned temp
      console.log(weather);
    }

  } catch (err) {
    // if error stop the loading indicator
    spinner.stop();
    // log the error
    console.error(err);
  }

}
