const menus = {
  main: `
    [command] <options>

    help ............... show help menu
    version ............ package version
    weather ............ get weather`,

  weather: `
    weather <options>

    --city, -c ............... specify city
    --zipcode, -z ............ specify zipcode
    --temp, -t ............... specify temperature units`,
}

module.exports = (args) => {
  const subCmd = args._[0] === 'help'
    ? args._[1]
    : args._[0];

  console.log(menus[subCmd] || menus.main);
}
