Weather CLI App
===================
> Weather CLI App that uses [OpenWeather API](https://openweathermap.org/api) to display the weather for a place.

Currently Implemented User Stories
-------------
-	User can query city by a zip code or city name
-	User is receives temperatures in ⁰C or ⁰F

Remaining User Stories to be Implemented
-------------
-	User can store the last config and provide a flag to use it
-	User can import a file to get the weather in max 10 cities (*Do not save configs for this use-case*)

How to Get Started
-------------
*NOTE: Currently Weather CLI App can only be used with Unix and Linux operating systems, if you are using a Windows machine please use a [Linux Subsystem](https://docs.microsoft.com/en-us/windows/wsl/install-win10)*
1)	Clone this repository
2) ```cd``` into the **directory**
3) **run** the command ```npm install```
4) From the **parent directory** apply the correct filesystem permissions:
	```chmod +x bin/weather```
5) From within the **parent directory** **run** the command ```bin/weather```
6) You will be **prompted** by the **app** to *enter a command*.


Commands and Options Available
-------------
```
    [command] <options>

    help ............... show help menu
    version ............ package version
    weather ............ get weather



    weather <options>

    --city, -c ............... specify city
    --zipcode, -z ............ specify zipcode
    --temp, -t ............... specify temperature units
```

Usage/Examples
-------------
*From within the parent directory, run the following command to start the Weather CLI:*
```bin/weather```

The app will display a *welcome banner* and **prompt** the user for a command.

![alt tag](images/img1.png)


*Get weather by city and country-code,  in Fahrenheit*
```
$ weather -c Paris,fr -t f

=> 31.24 °F
```

*Query weather by zipcode and country-code, in Celsius*
```
$ weather -z 94102,us -t c

=> 10.48 °C
```

*Get weather by city (without country-code), in Fahrenheit*
```
$ weather --city Paris --temp f

=> 31.24 °F
```

Technologies Used
-------------
-	[node.js](https://nodejs.org/en/)
-	[got - HTTP request library](https://www.npmjs.com/package/got)
-	[minimist - parse argument options](https://www.npmjs.com/package/minimist)
-	[inquirer - command line interface for node.js](https://www.npmjs.com/package/inquirer)
-	[ora - elegant terminal spinner](https://www.npmjs.com/package/ora)
-	[figlet - FIGfont spec in JavaScript](https://www.npmjs.com/package/figlet)
-	[chalk - terminal string styling](https://www.npmjs.com/package/chalk)
