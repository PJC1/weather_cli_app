const minimist = require('minimist');
const got = require('got');
const inquirer = require('inquirer');
const chalk = require('chalk');
const figlet = require('figlet');

// init function to display the welcome banner
const init = () => {
  console.log(
    chalk.green(
      figlet.textSync("weather cli", {
        font: "Standard",
        horizontalLayout: "default",
        verticalLayout: "default"
      })
    )
  );
}

// question function
const askQuestions = () => {
  // creating the array of object(s) containing the questions which will be consumed by inquirer
  const question = [
    {
      name: "WELCOME",
      type: "input",
      message: "Welcome to Weather CLI. Please enter a command..."
    }
  ];

  // use inquirer's prompt method to invoke the question(s)
  return inquirer.prompt(question);
};

module.exports = async () => {
  // output the welcome banner
  init();
  // ask question
  const answers = await askQuestions();
  // turn the answer into an array
  const answersArr = answers.WELCOME.split(' ');
  // caching the command/arguments
  const args = minimist(answersArr);
  // setting the command, if no command is given then default to 'help'
  let cmd = args._[0] || 'help';

  // handle version command
  if (args.version || args.v) {
    cmd = 'version';
  }

  // handle help command
  if (args.help || args.h) {
    cmd = 'help';
  }

  // switch statement for commands
  switch (cmd) {
    case 'weather':
      require('./cmds/weather')(args);
      break;

    case 'version':
      require('./cmds/version')(args);
      break;

    case 'help':
      require('./cmds/help')(args);
      break;

    default:
      console.error(`"${cmd}" is not a valid command!`);
      break;
  }
}
