const got = require('got');

module.exports = async (arg) => {
  // base url for openweather request
  let url = 'https://api.openweathermap.org/data/2.5/weather?appid=d800995b290947ec055fc167776c2447';
  // by default use °C, unless explicitly set with -t flag
  let units = 'metric';
  // by default use °C, keep track of units symbol used °C or °F
  let unitSymbol = '°C';

  // handle temp flag
  if(arg.hasOwnProperty('temp')) {
    // if the temp flag argument is 'f' then set units to imperial (°F), else use default (°C)
    if(arg.temp === 'f') {
			units = 'imperial';
			unitSymbol = '°F';
		} else if (arg.temp === 'c') {
			// default values for units and unitSymbol will be used
		}
	}

	try {
    // handle zipcode flag
    if(arg.hasOwnProperty('zipcode')) {
      // create an array containing the zipcode and countryCode
      const zipArr = arg.zipcode.split(',');
      // cache the zipcode
      const zip = zipArr[0];
      // cache the country code
      const countryCode = zipArr[1];
      // use got to make the http request to openweather api with the zipcode and country code
      const response = await got(url+'&zip='+zip+','+countryCode+'&units='+units);
      // use JSON.parse() on the requests' response in order to access the data
      const formatResponse = JSON.parse(response.body);
      // return the temp
      return `${formatResponse.main.temp} ${unitSymbol}`;
    }

    // handle city flag
    if(arg.hasOwnProperty('city')) {
      // check if city flag argument contains a coma (city + country-code)
      if(arg.city.includes(',')) {
        // create an array containing the city and countryCode
        const cityArr = arg.city.split(',');
        // cache the city
        const city = cityArr[0];
        // cache the country code
        const countryCode = cityArr[1];
        // make request to openweather
        const response = await got(url+'&q='+city+','+countryCode+'&units='+units);
        // format the response
        const formatResponse = JSON.parse(response.body);
        // return the temp
        return `${formatResponse.main.temp} ${unitSymbol}`;
      }

      // check if city flag argument has no country-code specified
      if(!arg.city.includes(',')) {
        // cache the city
        const city = arg.city;
        // make request to openweather
        const response = await got(url+'&q='+city+'&units='+units);
        // format the response
        const formatResponse = JSON.parse(response.body);
        // return the temp
        return `${formatResponse.main.temp} ${unitSymbol}`;
      }
    }
	} catch (error) {
    // if error, log it to the console
		console.log(error.response.body);
	}
};
